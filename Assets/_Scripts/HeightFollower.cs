using UnityEngine;

public class HeightFollower : MonoBehaviour
{
    public string playerName;

    Transform playerTransform;
    //float offset;

    void Start()
    {
        playerTransform = GameObject.Find(playerName).transform;
        //offset = transform.position.y - playerTransform.position.y;
    }

    void Update()
    {
        if (playerTransform == null) return;
        float x = playerTransform.position.x;
        float y = playerTransform.position.y;

        transform.position = new Vector3(x, y, transform.position.z);
    }
}
