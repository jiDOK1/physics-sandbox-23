using System;
using System.Collections.Generic;
using UnityEngine;

public class CollisionEvent : MonoBehaviour
{
    public static event Action<string> FatalCollision;
    public static event Action<string, float> BottomCollision;

    public List<Collider> cols;
    public List<Collider> bottomCols;

    private void OnCollisionEnter(Collision collision)
    {
        Collider other = collision.collider;
        if(cols.Contains(other))
        {
            FatalCollision?.Invoke(other.tag);
        }
        else if(bottomCols.Contains(other))
        {
            BottomCollision?.Invoke(other.tag, collision.relativeVelocity.magnitude);
        }
    }
}
