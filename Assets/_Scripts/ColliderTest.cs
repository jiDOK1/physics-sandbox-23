using UnityEngine;

public class ColliderTest : MonoBehaviour
{
    Rigidbody rb;
    Vector3 force;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            force = Vector3.up * 10f;
        }
    }

    void FixedUpdate()
    {
        rb.AddForce(force);
        force = Vector3.zero;
        float turn = Input.GetAxis("Vertical");
        rb.AddTorque(Vector3.right * 10f * turn);

        // don'ts:
        //transform.position += Time.deltaTime * 5f * Vector3.forward;

        //dos
        //rb.MovePosition(rb.position + Time.fixedDeltaTime * 5f * Vector3.forward);
        //Quaternion deltaRot = Quaternion.Euler(Vector3.right * 100f * Time.fixedDeltaTime);
        //rb.MoveRotation(rb.rotation * deltaRot);


        // funktioniert nicht:
        //if (Input.GetButtonDown("Jump"))
        //{
        //    rb.AddForce(Vector3.up * 10f, ForceMode.Impulse);
        //}

    }

    //void OnTriggerEnter(Collider other)
    //{
    //    Debug.Log("entered Trigger Zone");
    //    Debug.Log(other.name);
    //}
}
