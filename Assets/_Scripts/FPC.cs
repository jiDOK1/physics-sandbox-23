using UnityEngine;

public class FPC : MonoBehaviour
{
    public float walkSpeed = 50f;
    public float rotSpeed = 300f;
    public float jumpForce = 100f;
    public float maxAngle = 85f;
    Camera cam;
    float xAngle;
    float fwdForce;
    float strafeForce;
    float mouseX;
    float mouseY;
    Rigidbody rb;
    int jumpCounter;
    bool isGrounded;


    void Awake()
    {
        cam = GetComponentInChildren<Camera>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        fwdForce = Input.GetAxis("VerticalFPC");
        strafeForce = Input.GetAxis("HorizontalFPC");
        mouseX = Input.GetAxis("Mouse X");
        Quaternion deltaRot = Quaternion.Euler(mouseX * Vector3.up * rotSpeed * Time.deltaTime);
        rb.MoveRotation(rb.rotation * deltaRot);
        mouseY = Input.GetAxis("Mouse Y");
        xAngle -= mouseY * rotSpeed * Time.deltaTime;
        xAngle = Mathf.Clamp(xAngle, -maxAngle, maxAngle);
        cam.transform.localEulerAngles = Vector3.right * xAngle;
        if (Input.GetButtonDown("Jump") && jumpCounter < 1)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            jumpCounter++;
            //rb.velocity = Vector3.up * 3f;
        }
        if (isGrounded) jumpCounter = 0;
    }

    void FixedUpdate()
    {
        Vector3 camForward = new Vector3(cam.transform.forward.x, 0f, cam.transform.forward.z);
        camForward *= fwdForce;
        Vector3 camRight = new Vector3(cam.transform.right.x, 0f, cam.transform.right.z);
        camRight *= strafeForce;
        Vector3 dir = (camForward + camRight).normalized;
        rb.AddForce(walkSpeed * dir);
        Ray ray = new Ray(transform.position + Vector3.up, Vector3.down);
        isGrounded = Physics.Raycast(ray, 1.01f);
    }
}
