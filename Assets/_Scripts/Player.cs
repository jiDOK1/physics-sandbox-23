using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public string axisVertical;
    public string axisHorizontal;
    public float thrust = 10f;
    public float rotSpeed = 10f;

    Vector3 force;
    float turn;
    Rigidbody rb;

    void Awake()
    {
        CollisionEvent.FatalCollision += OnFatalCollision;
        CollisionEvent.BottomCollision += OnBottomCollision;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        force = Mathf.Abs(Input.GetAxis(axisVertical)) * Vector3.up;
        turn = Input.GetAxis(axisHorizontal);
    }

    void FixedUpdate()
    {
        rb.AddRelativeForce(force * thrust, ForceMode.Acceleration);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, 15f);
        Quaternion deltaRot = Quaternion.Euler(-turn * Vector3.forward * rotSpeed * Time.fixedDeltaTime);
        rb.MoveRotation(rb.rotation * deltaRot);
        //rb.AddTorque(turn * Vector3.forward * rotSpeed);
    }

    void OnFatalCollision(string tag)
    {
        if (gameObject.CompareTag(tag))
        {
            //Destroy(gameObject);
            SceneManager.LoadScene(0);
        }
    }

    void OnBottomCollision(string tag, float relVelo)
    {
        if (gameObject.CompareTag(tag) && relVelo > 8)
        {
            //Destroy(gameObject);
            SceneManager.LoadScene(0);
        }
    }

    void OnDestroy()
    {
        CollisionEvent.BottomCollision -= OnBottomCollision;
        CollisionEvent.FatalCollision -= OnFatalCollision;
    }
}
